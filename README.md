# Presentations

A simple repo for my presentations and talks, all material is copyrighted but released under the MIT licence.

Two sub folders - for PDFs and plain text (for accessibility purposes)